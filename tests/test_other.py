#!/usr/bin/env python3
"""Test refresh"""

from pathlib import Path
from shutil import rmtree
from time import sleep
from unittest import main, TestCase
from pexpect import spawn
from test_normal import get_keys


class TestProcess(TestCase):
    """Test"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.keys_q = get_keys("quit")
        self.keys_d = get_keys("go to the folder of the file selected")
        self.keys_e = get_keys("refresh current folder")

    def test_00_refresh(self) -> None:
        """00. Refresh current folder"""

        key_q = self.keys_q[0]
        for key_e in self.keys_e:
            # Create folders
            dir0 = Path("a")
            dir0.mkdir(parents=True)
            # Create files under path
            for i in range(10):
                fil = dir0 / f"zzz{i}zzz"
                fil.write_text(f"Text {i}", encoding="UTF-8")
            # Open
            proc = spawn(
                f"coverage run --parallel-mode sf.py '{dir0}'",
                encoding="UTF8",
                env={"TERM": "xterm-256color"},
            )
            sleep(1)
            # Remove last file
            fil.unlink()
            # Refresh
            keys = f"{key_e}{key_q}"
            proc.sendline(keys)
            out = proc.read()
            proc.close()
            # Check
            self.assertEqual(proc.exitstatus, 0)
            page1 = out.split("\x1b[44m")[1].splitlines()
            self.assertEqual(len(page1), 10 - 1)
            page2 = out.split("\x1b[44m")[2].splitlines()
            self.assertEqual(len(page2), 9)
            # Clean
            rmtree(dir0)

    def test_01_fzf(self) -> None:
        """01. Go to folder of the file selected"""

        key_q = self.keys_q[0]
        for key_d in self.keys_d:
            # Create folders
            dir0 = Path("a")
            dir0.mkdir(parents=True)
            dir1 = dir0 / "b"
            dir1.mkdir(parents=True)
            # Create files under path
            for i in range(10):
                (dir0 / f"aaa{i}").write_text(f"Text a {i}", encoding="UTF-8")
            for i in range(20):
                (dir1 / f"bbb{i}").write_text(f"Text b {i}", encoding="UTF-8")
            # dir0
            for case in [dir0, "aaa0"]:
                proc = spawn(
                    "coverage run --parallel-mode sf.py",
                    encoding="UTF-8",
                    env={"TERM": "xterm-256color"},
                )
                proc.sendline(f"{key_d}{case}")
                sleep(0.5)
                proc.sendline("\r")
                sleep(0.5)
                proc.sendline(f"{key_q}")
                out = proc.read()
                proc.close()
                # Check
                self.assertEqual(proc.exitstatus, 0)
                page = out.rsplit("\x1b[44m", maxsplit=1)[-1].splitlines()
                self.assertEqual(len(page), 11)
                self.assertIn("aaa0", page[0])
            # dir1
            for case in [dir1, "bbb0"]:
                proc = spawn(
                    "coverage run --parallel-mode sf.py",
                    encoding="UTF-8",
                    env={"TERM": "xterm-256color"},
                )
                proc.sendline(f"{key_d}{case}")
                sleep(0.5)
                proc.sendline("\r")
                sleep(0.5)
                proc.sendline(f"{key_q}")
                out = proc.read()
                proc.close()
                # Check
                self.assertEqual(proc.exitstatus, 0)
                page = out.rsplit("\x1b[44m", maxsplit=1)[-1].splitlines()
                self.assertEqual(len(page), 20)
                self.assertIn("bbb0", page[0])
            # Clean
            rmtree(dir0)


if __name__ == "__main__":
    main()
