# sf

A simple console file manager written in `python`.

[![pipeline status](https://gitlab.com/yellowhat-labs/sf/badges/main/pipeline.svg)](https://gitlab.com/yellowhat-labs/sf/-/commits/main)
[![coverage report](https://gitlab.com/yellowhat-labs/sf/badges/main/coverage.svg)](https://gitlab.com/yellowhat-labs/sf/-/commits/main)
[![Latest Release](https://gitlab.com/yellowhat-labs/sf/-/badges/release.svg)](https://gitlab.com/yellowhat-labs/sf/-/releases)
[![asciicast](https://asciinema.org/a/383716.svg)](https://asciinema.org/a/383716)

## Table of Contents

<!-- vim-markdown-toc GFM -->

* [Dependencies](#dependencies)
* [Installation](#installation)
* [Usage](#usage)
* [Features](#features)
* [Keybindings](#keybindings)

<!-- vim-markdown-toc -->

## Dependencies

* `python 3.12+`
* `coreutils` (file operations)
* `fzf` (optional)

## Installation

cURL `sf` to your `$PATH` and give execute permissions.

```bash
sudo curl \
    --location \
    --output /usr/local/bin/sf \
    https://gitlab.com/yellowhat-labs/sf/-/raw/main/sf.py
sudo chmod +x /usr/local/bin/sf
```

## Usage

```bash
sf --help
```

## Features

* Move between folders (parent/child/bookmarks)
* Create new file/folder
* Delete files/folders
* Copy/Move files/folders and/or between multiple instances
* Rename files/folders in the current view
* Search in the current folder
* Go to the folder of the file selected using `fzf`

## Keybindings

This is the list of full keybindings along with their default values.

To modify the default values change the `self.keybindings` dictionary.

| Key       | Action                                |
|-----------|---------------------------------------|
| ?         | show keybindings                      |
| j         | scroll down                           |
| k         | scroll up                             |
| h         | go to parent folder                   |
| l         | go to child folder/open file          |
| down      | scroll down                           |
| up        | scroll up                             |
| left      | go to parent folder                   |
| right     | go to child folder/open file          |
| enter     | go to child folder/open file          |
| backspace | go to parent folder                   |
| g         | go to top                             |
| G         | go to bottom                          |
| .         | toggle hidden files                   |
| e         | refresh current folder                |
| d         | go to the folder of the file selected |
| f         | new file                              |
| n         | new folder                            |
| r         | rename all items in current folder    |
| ~         | go to home                            |
| !         | open a terminal in the current folder |
| /         | search in the current folder          |
| [1-9]     | bookmark examples                     |
| p         | mark current item                     |
| space     | mark current item                     |
| P         | mark all items in current folder      |
| c         | clear marked items list               |
| y         | copy marked items to current folder   |
| m         | move marked items to current folder   |
| x         | delete marked items                   |
| q         | quit                                  |

On pressing `y`, `m` or `x`, if the marked items list is empty,
it will consider the current selected item for the operation.
